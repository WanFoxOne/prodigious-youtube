// ==UserScript==
// @name            Prodigious YouTube
// @description     Coucou ! Une description inutile ! (Comme vous et moi)
// @author          WanFoxOne <contact@corler.pro>
// @icon            https://s.ytimg.com/yts/img/favicon_32-vflOogEID.png
// @homepageURL     https://gitlab.com/wanfoxone
// @namespace       https://corler.pro
// @version         1
// @include         http*://*.youtube.com/*
// @include         http*://youtube.com/*
// @include         http*://*.youtu.be/*
// @include         http*://youtu.be/*
// @grant           none
// ==/UserScript==

(function (document) {
  "use strict";
  
  // Is iframe ?
  function inIframe () {
    try {
      return window.self !== window.top;
    } catch (e) {
      return true;
    }
  }
  if (inIframe()) return;
  
  // -------------------------------------------------------------------------------------------------------------------------------------
  
  let target = document.getElementsByTagName('ytd-two-column-browse-results-renderer');
  
  if(target.length) {
    
    Object.assign(target[0].style,{
      width: '100%',
      padding: '0 20px',
      boxSizing: 'border-box'
    });
  }
  
  // -------------------------------------------------------------------------------------------------------------------------------------
  
  const container = document.querySelectorAll('#contents.ytd-section-list-renderer')[0];
  
  if(!container) return;
  
  const preview_observer = new MutationObserver(set_preview);
  preview_observer.observe(container, { attributes: true, childList: true, subtree: false });
  
  window.onload = set_preview;

  
  function set_preview() {
        
    let thumbnails = document.getElementsByTagName('ytd-thumbnail');
		
    for(const thumbnail of thumbnails) {
			
      if(thumbnail.hasAttribute('preview')) return;
      console.log("OK");
      const progress = document.createElement('preview_progress');

      Object.assign(progress.style,{
        width: 0,
        height: '3px',
        backgroundColor: '#EB9532',
        position: 'absolute',
        zIndex: 1,
        top: 0,
        left: 0,
        pointerEvents: 'none',
        transition: '100ms'
      });

      thumbnail.prepend(progress);

      thumbnail.addEventListener('mouseenter', function(event) {
        event.target.setAttribute('preview', 0);
        preview(thumbnail);
      });

      thumbnail.addEventListener('mouseleave', function(event) {
        event.target.setAttribute('preview', false);
        preview(thumbnail);
      });
    }

    let preview = function(thumbnail){

      const regex_extract_id = /(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;

      const id = thumbnail.getElementsByClassName('yt-simple-endpoint')[0].href.match(regex_extract_id);
      const image = thumbnail.getElementsByTagName('img');
      const state = parseInt(thumbnail.getAttribute('preview'));
      let progress = thumbnail.getElementsByTagName('preview_progress')[0];

      if(id.length > 1 && image.length && progress) {

        if(state >= 0) {

          const new_state = (state >= 3) ? 1 : state + 1 || false;
          thumbnail.setAttribute('preview', new_state);
          
          Object.assign(progress.style,{
            width: `calc((100% / 3) * ${new_state})`,
          });

          image[0].setAttribute('src', `https://i.ytimg.com/vi/${id[1]}/${new_state}.jpg`);

          if(window[`yt_preview${id}`]) window.clearTimeout(window[`yt_preview${id}`]);

          window[`yt_preview${id}`] = window.setTimeout(function() {
            preview(thumbnail);
          },700);

        } else {

          Object.assign(progress.style,{
            width: 0,
          });
          
          window.clearTimeout(window[`yt_preview${id}`]);
          image[0].setAttribute('src', `https://i.ytimg.com/vi/${id[1]}/hqdefault.jpg`);
        }   
      }
    }
  }
  
})(document);